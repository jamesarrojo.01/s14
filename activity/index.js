// MY SOLUTION

function add(a, b) {
	console.log("The sum of " + a + " and " + b + " is:");
	console.log(a + b);
}

// calling the function

add(56, 89);

// subtract two numbers

function subtract(a, b) {
	console.log("The difference of " + a + " and " + b + " is:");
	console.log(a - b);
}

subtract(5688, 155);


// multiply two numbers

function multiply(a, b) {
	return a * b;
}

let a = 125;
let b = 17;

let product = multiply(a, b);
console.log("The product of " + a + " and " + b + " is:\n" + product);


// solution ni madam

function addNum(num1, num2) {
	console.log(num1 + num2);
}

addNum(3, 4);

function subtractNum(num1, num2) {
	console.log(num1 - num2);
}

subtractNum(10, 6);

function multiplicationAns(num1, num2) {
	// string literals ${}
	return `${num1} ${num2}`
}

let product2 = multiplicationAns(2 * 8)
console.log(product2);

// function mul(x, y) {
// 	console.log(x * y);
// }

// mul(5, 2);

function mul(x, y) {
	return x * y;
}

let product3 = mul(5, 10);
console.log(product3);